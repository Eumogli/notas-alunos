using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notas_Alunos
{
    class Program
    {

        static void Main(string[] args)
        {
            int opcao=0;
            float soma = 0;
            float[] notas = new float[9];
            int AlunosNotaNegativa = 0;
            bool validar = false;
            for (int i = 0; i < 9; i++)
            {
                validar = false;
                while (!validar)
                {
                    Console.Write($"Insira a nota {i + 1}:");

                    try
                    {
                        validar = true;
                        notas[i] = float.Parse(Console.ReadLine()
                        .Replace(".", ","));
                        validar = notas[i] <= 20 && notas[i] >= 0;

                    }
                    catch (Exception)
                    {
                            validar = false;
                   
                    }
                }
                soma += notas[i];
                if (notas[i] < 10)
                    AlunosNotaNegativa++;

                Console.Clear();

            }

            do
            {
                validar = false;
                while (!validar)
                {
                    Console.Clear();
                    Console.WriteLine("[ 7 ] Calcular média das notas");
                    Console.WriteLine("[ 6 ] Numero de notas acima da média");
                    Console.WriteLine("[ 5 ] Maior nota");
                    Console.WriteLine("[ 4 ] Número de alunos com a maior nota");
                    Console.WriteLine("[ 3 ] Menor nota");
                    Console.WriteLine("[ 2 ] Número de alunos com a menor nota");
                    Console.WriteLine("[ 1 ] Número de alunos com nota negativa");
                    Console.WriteLine("[ 0 ] Sair do Programa");
                    Console.WriteLine("-------------------------------------");
                    Console.Write("Insira uma opção: ");
                    try
                    {
                        validar = true;
                        opcao = Int32.Parse(Console.ReadLine());

                    }
                    catch (Exception)
                    {
                        validar = false;
                    }
                }

                switch (opcao)
                {
                    case 7:
                        Console.WriteLine($"Escolheu a opção: Calcular média\nA média é: {(notas.Average()):0.00}");
                        break;
                    case 6:
                        Console.WriteLine($"Escolheu a opção: Numero de notas acima da média\nNumero de notas acima da média é : {notas.Count(x => x > notas.Average())}");
                        break;
                    case 5:
                        Console.WriteLine($"Escolheu a opção: Maior nota\nA maior nota é: {notas.Max()}");
                        break;
                    case 4:
                        Console.WriteLine($"Escolheu a opção: Alunos com maior nota\nNúmero de alunos com a maior nota: {notas.Count(x => x== notas.Max())}");
                        break;
                    case 3:
                        Console.WriteLine($"Escolheu a opção: Menor nota\nA menor nota é: {notas.Min()}");
                        break;
                    case 2:
                        Console.WriteLine($"Escolheu a opção: Alunos com a menor nota\nNúmero de alunos com a menor nota: {notas.Count(x => x == notas.Min())}");
                        break;
                    case 1:
                        Console.WriteLine($"Escolheu a opção: Alunos com nota negativa\nO numero de alunos com nota negativa são:{AlunosNotaNegativa}");
                        break;
                    case 0:
                        saiPrograma();
                        break;
                    default: Console.Write("\t\tOpção inválida!");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
            while (opcao != 0);
        }

        static void saiPrograma()
        {
            Console.WriteLine();
            Console.WriteLine("Até breve, saiu do Programa. Prima qualquer tecla para sair...");
        }

        
    }
}

